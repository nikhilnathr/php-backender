<?php

class Response {
  
  function send($data = "") {
    $this->send_code(200, $data);
  }

  function send_code($status_code, $data = "") {
    http_response_code($status_code);
    require_once(__DIR__ . "/../includes/headers.php");
    if ( ($json = json_encode($data, JSON_UNESCAPED_SLASHES)) !== false && ($json[0] === "{" || $json[0] === "[") ) {
      echo $json;
    } else {
      echo $data;
    }
    exit();
  }

    function not_found($msg = "") {
      $this->send(404, $msg);
    }
  }